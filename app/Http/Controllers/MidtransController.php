<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Midtrans;

class MidtransController extends Controller
{
    public function generate()
    {
        Midtrans\Config::$serverKey = config('app.midtrans.server_key');

        Midtrans\Config::$isSanitized = true;
        Midtrans\Config::$is3ds = true;

        $params = [
            'transaction_details' => [
                'order_id' => "order-101",
                'gross_amount' => 44000
            ],
            'customer_details' => [
                'first_name'    => null,
                'last_name'     => null,
                'email'         => "ismayanareggy@gmail.com",
                'phone'         => null
            ]
        ];

        $midtrans_transaction = Midtrans\Snap::createTransaction($params);
        $token_snap = $midtrans_transaction->token;
        $paymentUrl = $midtrans_transaction->redirect_url;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'success',
            'data' => $midtrans_transaction
        ]);
    }
}
